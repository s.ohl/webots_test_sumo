# Copyright 1996-2021 Cyberbotics Ltd.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""ROS2 Tesla driver."""

from turtle import rt
import rclpy
from ackermann_msgs.msg import AckermannDrive
from std_msgs.msg import Float32
from webots_interfaces.msg import RadarTarget
from pprint import pprint, pformat


class TeslaDriver:

    def init(self, webots_node, properties):
        self.__robot = webots_node.robot

        self.vehicle_topic = properties["vehicle"]

        # ROS interface
        rclpy.init(args=None)
        self.__node = rclpy.create_node('tesla_node')
        self.__node.create_subscription(AckermannDrive,
                                        self.vehicle_topic + '/cmd_ackermann',
                                        self.__cmd_ackermann_callback, 1)
        self.__node.create_subscription(Float32,
                                        self.vehicle_topic + '/cmd_steering',
                                        self.__cmd_steering_callback, 1)
        self.__node.create_subscription(Float32,
                                        self.vehicle_topic + '/cmd_speed',
                                        self.__cmd_speed_callback, 1)

        self.__radar = self.__robot.getDevice("sms")
        if self.__radar is not None:
            self.__radar_publisher = self.__node.create_publisher(
                RadarTarget, self.vehicle_topic + '/radar_targets', 1)
            self.__radar.enable(1)

    def __cmd_ackermann_callback(self, message):
        self.__robot.setCruisingSpeed(message.speed)
        self.__robot.setSteeringAngle(message.steering_angle)

    def __cmd_steering_callback(self, message):
        self.__robot.setSteeringAngle(message.data)

    def __cmd_speed_callback(self, message):
        self.__robot.setCruisingSpeed(message.data)

    def step(self):
        if self.__radar is not None:
            for t in self.__radar.getTargets():
                rt = RadarTarget()
                rt.header.stamp = self.__node.get_clock().now().to_msg()
                rt.distance = t.distance
                rt.receiver_power = t.receiver_power
                rt.speed = t.speed
                rt.azimuth = t.azimuth
                self.__radar_publisher.publish(rt)

        rclpy.spin_once(self.__node, timeout_sec=0)
